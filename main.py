import pandas as pd
import sys

import boom


BOOM_FILE = 'inputs/BOM.XLSX'
# TODO: Parameter input files


if __name__ == "__main__":
    unused_cols = ['MType', 'MGrp', 'PGrp', 'Quantity', 'UoM', 'Moving price', 'Price unit']

    df_bom = pd.read_excel(open(BOOM_FILE, mode='rb')) \
             .drop(unused_cols, axis=1) \
             .fillna(0)

    bom_processed = boom.preprocess_bom(df_bom)
    as_for_excel = boom.as_for_excel(bom_processed)
