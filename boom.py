import copy as cp
import numpy as np
import pandas as pd


DAY_OFFSET = 9
OUTPUT_FI = "Generated.xlsx"
# TODO: parameter days of month
DAYS_OF_MONTH = 31
days_of_month = list(map(lambda e: e + 1, range(DAYS_OF_MONTH)))
MASTER_FILE_INFO_COLS = ['LINE', '', 'OWNER', 'SIZE', 'TYPE', 'GEOMETRY', "Q'TY", 'Suspension', 'Liftgate', 'Reefer Unit', 'MODEL NO.', 'LOT']
MASTER_FILE_CALCULATED_COLS = ["", "", "Vendor", "SAP", "Description", "QTY", "Total"]
GENERATED_EXCEL_FINAL_HEADS = MASTER_FILE_INFO_COLS + days_of_month + MASTER_FILE_CALCULATED_COLS
KEY_COLS = ['Component', 'Description']


def preprocess_bom(df_bom):
    models = []

    # Classify models with materials
    for column in df_bom.columns:
        if column in KEY_COLS:
            continue

        with_mat = df_bom.loc[df_bom[column] > 0][KEY_COLS + [column]]
        if with_mat.empty:
            continue

        models.append(with_mat)

    # Summarize quantities
    for i in range(len(models)):
        model = models[i]
        models[i] = model.groupby(KEY_COLS).sum().reset_index()

    bom_processed = pd.concat(models).fillna(0)

    return bom_processed


def as_for_excel(bom, nobom):
    df_master = nobom

    column_k = 'Unnamed: 10'  # Column that contains the MODEL NO. [f'Unnamed: {i}' for i in range(5, 12)]
    df_master[column_k] = df_master[column_k].values.astype(str)  # Change type of the MODEL NO column
    print("=== nobom before")
    print(df_master[column_k].head(8))
    print()

    master_rows = df_master.loc[df_master[column_k].str.startswith('300', na=False)]  # Get only models of interest
    if master_rows.index.size == 0:
        print('Warning: There is no record starting with 300 in the column MODEL NO')

    print("=== nobom after")
    print(master_rows.index.size)
    print(df_master[column_k].head(8))
    master_rows.to_csv('master.csv', index=False)

    # Process each master excel rows and...
    created_rows = []
    for _, master_row in master_rows.iterrows():
        print('entered!')
        model_no = str(master_row[column_k])
        filtered_bom = pd.DataFrame()

        if model_no in bom.columns:
            # Gets model components whose usage is greather than 0
            filtered_bom = bom.loc[bom[model_no] > 0]

        # Creates template rows
        row_tmpx = fill_info_columns(master_row, ["" for i in range(len(GENERATED_EXCEL_FINAL_HEADS))])
        row_tmpx = fill_last_columns(filtered_bom, row_tmpx, model_no)
        { created_rows.append(row) for row in row_tmpx }

    return (created_rows, GENERATED_EXCEL_FINAL_HEADS)


def fill_last_columns(filtered_bom, target, model_no):
    clons = []
    offset = len(MASTER_FILE_INFO_COLS + days_of_month) + MASTER_FILE_CALCULATED_COLS.index('SAP')

    if filtered_bom.empty:
        _target = cp.deepcopy(target)
        _target[offset+1] = f"<< No data for this model has been found from the given BOM >>"

        clons.append(_target)

    for _, source_row in filtered_bom.iterrows():
        # Copy template: Clone it
        _target = cp.deepcopy(target)

        # Assign SAP
        _target[offset] = source_row['Component']

        # Assign Description
        _target[offset+1] = source_row['Description']

        # Assign Quantity
        _target[offset+2] = source_row[model_no]

        # Assign Total
        _target[offset+3] = float(_target[offset+2]) * float(_target[GENERATED_EXCEL_FINAL_HEADS.index("Q'TY")])

        clons.append(_target)

    return clons


def fill_info_columns(source, target, line=None):
    # TODO: Parameter the 31
    _SOURCE_COLS = [(f"Unnamed: {i}", i) for i in range(0, 12 + DAYS_OF_MONTH)]
    for col, order in _SOURCE_COLS:
        if col in source.index:
            target[order] = source[col]
    return target


def generate_df(row_list, columns):
    df = pd.DataFrame(row_list)
    df.columns = columns
    print(df)
    df.to_excel(OUTPUT_FI, index=False)


def excel_tap_index_parser(tab_no):
    return tab_no - 1 if isinstance(tab_no, int) else tab_no
